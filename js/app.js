
const items = document.getElementById('items');
const templateCard = document.getElementById('template-card').content;
const contadores = document.getElementById('contadores')
const templateCount = document.getElementById('template-count').content;
const fragment = document.createDocumentFragment();



document.addEventListener('DOMContentLoaded', () => {
    fetchData();
});

const fetchData = async () => {
    try {
        const response = await fetch('Json/productos.json'); // Reemplaza con la ruta correcta de tu archivo JSON
        const data = await response.json();
        pintarCards(data.productos);
    } catch (error) {
        console.log(error);
    }
};

const pintarCards = (productos) => {
    productos.forEach((producto) => {
        const clone = templateCard.cloneNode(true);
        const card = clone.querySelector('.card');
        const img = card.querySelector('.card-img-top');
        const title = card.querySelector('.card-title');
        const type = card.querySelector('.card-text:nth-child(2)');
        const price = card.querySelector('.card-text:nth-child(3)');
        const buyButton = card.querySelector('.btn');

        img.src = producto.img;
        title.textContent = producto.titulo;
        type.textContent = `Tipo: ${producto.tipo}`;
        price.textContent = `Precio: $${producto.precio.toFixed(2)}`;
        buyButton.dataset.id = producto.id;

        fragment.appendChild(clone);
    });

    items.innerHTML = '';
    items.appendChild(fragment);
};


const pintarcont = () => {
    console.log(contador)
    contadores.innerHTML = ''
    Object.values(contador).forEach(producto => {
        templateCount.querySelector('').textContent = producto.titulo

        const clone = templateCont.cloneNode(true)
        fragment.appendChild(clone)
    })
    contadores.appendChild(clone)
}





items.addEventListener('click', e => {
    addContador(e)
})

const addContador = e => {
    console.log(e.target.classList.contains('btn-primary'))
    if (e.target.classList.contains('btn-primary')) {
        //console.log(e.target.parentElement)
        setContador(e.target.parentElement)
    }
}
const contador = 0;

const setContador = objeto => {
    console.log(objeto)
    const producto = {
        id: objeto.querySelector('.btn-primary').dataset.id,
        titulo: objeto.querySelector('h5').textContent,
        cantidad: 1
    }
    if (contador.hasOwnProperty(producto.id)) {
        producto.cantidad = contador[producto.id].cantidad + 1
    }
    contador[producto.id] = { ...producto }

    console.log(producto)
}